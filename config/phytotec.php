<?php

return [
    'departments' => [
        'System Administration',
        'Administration',
        'Research & Development',
        'Production & Maintenance',
        'Business Development',
        'Finance',
    ],

    'products' => [
        'Prostat60',
        'Demaphyte',
        'Omega-Zee',
        'ZetaPRO',
    ],

    'product_types' => [
        'Pack of 10',
        'Pack of 30',
        'Bottle',
    ],

    'medical' => [
        'specialties' => [
            'Urologist', 
            'Dermatologist', 
            'Herbal Doctor', 
            'General Physician', 
            'Physician Assistant',
            'Physician Specialist',
            'Pharmacist',
            'Medicine Counter Assistant'
        ]
    ]
    
    // 'departments' => [
    //     'Administration' => [
    //         'Administration',
    //         'Customer Care',
    //         'Public Relations',
    //     ],

    //     'Research & Development' => [
    //         'Product Development',
    //         'Production Standards',
    //         'Quality Control',
    //         'Quality Assurance',
    //         'Certifications',
    //         'Training',
    //     ],

    //     'Production & Maintenance' => [
    //         'Production',
    //         'Maintenance',
    //     ],

    //     'Business Development' => [
    //         'Marketing',
    //         'Sales',
    //         'Medical Services',
    //         'Media',
    //     ],

    //     'Finance' => [
    //         'Accounts',
    //         'Procurements',
    //         'Stores',
    //     ],
    // ],

];