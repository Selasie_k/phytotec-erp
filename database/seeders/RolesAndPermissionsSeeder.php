<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                // Reset cached roles and permissions
                app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

                // create permissions
                Permission::findOrCreate('edit articles');
                Permission::findOrCreate('delete articles');
                Permission::findOrCreate('publish articles');
                Permission::findOrCreate('unpublish articles');
        
                // create roles and assign created permissions

                $role = Role::findOrCreate('moderator')
                    ->givePermissionTo(['publish articles', 'unpublish articles']);
        
                $role = Role::findOrCreate('super-admin')
                    ->givePermissionTo(Permission::all());
    }
}
