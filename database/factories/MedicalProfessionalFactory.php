<?php

namespace Database\Factories;

use App\Models\MedicalProfessional;
use Illuminate\Database\Eloquent\Factories\Factory;

class MedicalProfessionalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MedicalProfessional::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
