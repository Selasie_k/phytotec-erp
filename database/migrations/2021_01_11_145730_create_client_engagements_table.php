<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientEngagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_engagements', function (Blueprint $table) {
            $table->id();
            $table->foreignId('sales_outlet_id')->constrained()->onDelete('cascade');
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->boolean('sells_products');
            $table->text('products_sold')->nullable();
            $table->string('reason_for_not_selling')->nullable(); // dropdown
            $table->text('promotional_items')->nullable(); // dropdown
            $table->string('product_visibility')->nullable();
            $table->string('supplier_type')->nullable(); // sales rep or wholesale outlet
            $table->string('supplier_name')->nullable();
            $table->integer('estimated_sales_volume')->nullable();
            $table->string('customer_purchase_mode')->nullable(); // with or without subscription or both
            $table->boolean('recommends_products')->nullable(); //
            $table->text('customer_feedback')->nullable(); // array
            $table->text('remarks')->nullable(); // dropdown
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_engagements');
    }
}
