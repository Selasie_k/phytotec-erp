<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalEngagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_engagements', function (Blueprint $table) {
            $table->id();
            $table->foreignId('professional_id');
            $table->string('name')->nullable(); // town / doctor
            $table->string('purpose'); // medical screening, Prescriber engagement
            $table->string('product')->nullable();
            // $table->integer('samples_issued')->nullable();
            // $table->integer('individuals_screened')->nullable();
            // $table->integer('subscriptions_issued')->nullable();
            $table->string('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_engagements');
    }
}
