<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('sale_id');
            $table->foreignId('user_id')->nullable();
            $table->string('method')->after('date'); // cash, check
            $table->string('check_number')->after('method'); // cash, check
            $table->date('check_due_date')->after('method')->nullable(); // cash, check
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->integer('sale_id');
            $table->dropColumn(['user_id', 'method', 'check_due_date', 'check_number']);
        });
    }
}
