<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditMedicalFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('health_facilities', function (Blueprint $table) {
            $table->string('digital_address')->nullable()->change();
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->string('check_number')->nullable()->change();
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->timestamp('check_cleared_on')->nullable()->after('check_due_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('health_facilities', function (Blueprint $table) {
            //
        });

        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('check_cleared_at');
        });
    }
}
