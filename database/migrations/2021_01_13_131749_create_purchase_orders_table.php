<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('sales_outlet_id')->constrained()->onDelete('cascade');
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->timestamp('delivery_date')->nullable(); // date & time
            $table->string('payment_term'); //cash, 7, 15, 30, 45, 60, [other] days
            // $table->date('payment_due_by')->nullable(); // computed
            $table->integer('invoice_number')->nullable(); // 
            $table->text('remarks')->nullable(); // 
            $table->timestamp('confirmed_at')->nullable(); // 
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
