<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalProfessionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_professionals', function (Blueprint $table) {
            $table->id();
            $table->foreignId('health_facility_id')->constrained();
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->string('specialty');
            $table->text('products_prescribed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_professionals');
    }
}
