<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProductsToSalesOutletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_outlets', function (Blueprint $table) {
            $table->text('products_stocking')->after('email');
            $table->text('wholesale_type')->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_outlets', function (Blueprint $table) {
            $table->dropColumn(['products_stocking', 'wholesale_type']);
        });
    }
}
