<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_surveys', function (Blueprint $table) {
            $table->id();
            $table->string('product');
            $table->foreignId('outlet_id')->constrained('sales_outlets');
            $table->foreignId('user_id')->constrained('users');
            $table->boolean('has_product');
            $table->integer('quantity');
            $table->integer('price');
            $table->text('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_surveys');
    }
}
