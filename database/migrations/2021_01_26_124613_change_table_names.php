<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTableNames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('callers', 'call_records');
        Schema::rename('reasons', 'call_reasons');
        Schema::rename('sources', 'ad_sources');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('call_records', 'callers');
        Schema::rename('call_reasons', 'reasons');
        Schema::rename('ad_sources', 'sources');
    }
}
