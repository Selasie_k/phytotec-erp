<?php

namespace App\Providers;

use App\Events\PurchaseOrderCreated;
use App\Events\PurchaseRequestCreated;
use App\Listeners\NotifyStaffOfNewPurchaseOrder;
use App\Listeners\NotifyStaffOfNewPurchaseRequest;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        PurchaseOrderCreated::class => [
            NotifyStaffOfNewPurchaseOrder::class
        ],

        PurchaseRequestCreated::class => [
            NotifyStaffOfNewPurchaseRequest::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
