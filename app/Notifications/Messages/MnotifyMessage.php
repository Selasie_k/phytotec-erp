<?php

namespace App\Notifications\Messages;


class MnotifyMessage
{
    public $from;

    public $to;

    public $content;

    public function __construct()
    {
        $this->from = 'PhytotecLTD';
    }

    public function from($from)
    {
        $this->from = $from;

        return $this;
    }

    public function to($to = null)
    {
        $this->to = $to;

        return $this;
    }

    public function content($content)
    {
        $this->content = $content;

        return $this;
    }
}