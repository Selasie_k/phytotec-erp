<?php
namespace App\Notifications\Channels;

use Illuminate\Notifications\Notification;
use App\Services\MNotifyService;

class MnotifyChannel 
{
    protected $mNotify;

    public function __construct(MNotifyService $service)
    {
        $this->mNotify = $service;
    }

    public function send($notifiable, Notification $notification)
    {
        if (! $to = $notifiable->routeNotificationFor('mNotify', $notification)) {
            return;
        }

        $message = $notification->toMNotify($notifiable);

        return $this->mNotify
                ->from($message->from)
                ->to($to)
                ->message($message->content)
                ->send();
    }
}
