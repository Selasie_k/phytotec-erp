<?php

namespace App\Notifications;

use App\Models\PurchaseOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\Messages\MnotifyMessage;
use Illuminate\Notifications\Messages\MailMessage;

class PurchaseOrderNotificationToStaff extends Notification
{
    use Queueable;

    public $purchaseOrder;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(PurchaseOrder $purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mNotify'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toMNotify($notifiable)
    {
        return (new MnotifyMessage)
                ->content("Purchase order created for {$this->purchaseOrder->outlet->name} by {$this->purchaseOrder->user->name}");
    }
}
