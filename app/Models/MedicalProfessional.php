<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Propaganistas\LaravelPhone\PhoneNumber;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MedicalProfessional extends Model
{
    use HasFactory;

    protected $casts = [
        'products_prescribed' => 'array'
    ];

    public function getPhoneAttribute($value)
    {
        return (string) PhoneNumber::make($value, 'GH'); 
    }

    public function health_facility()
    {
        return $this->belongsTo(HealthFacility::class, 'health_facility_id');
    }

    public function healthFacilities()
    {
        return $this->belongsToMany(HealthFacility::class, 'facility_professional', 'medical_professional_id', 'health_facility_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeFilter($query, array $filters)
    {
        $query
        ->when($filters['name'] ?? null, function ($query, $name) {
            $query->where('name', 'LIKE', "%$name%");
        })->when($filters['product'] ?? null, function ($query, $product) {
            $product == 'Not Prescribing' 
                ? $query->where('products_prescribed', "[]")
                : $query->where('products_prescribed', 'LIKE', "%$product%");
        })->when($filters['specialty'] ?? null, function ($query, $specialty) {
            $query->where('specialty', 'LIKE', "%$specialty%");
        })->when($filters['user_id'] ?? null, function ($query, $user_id) {
            $query->where('user_id', $user_id);
        })->when($filters['created_at'] ?? null, function ($query, $created_at) {
            $query->whereBetween('created_at', [$created_at[0], $created_at[1] ?? now()]);
        });
    }
}
