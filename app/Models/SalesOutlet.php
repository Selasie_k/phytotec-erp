<?php

namespace App\Models;

use Faker\Provider\ar_SA\Payment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesOutlet extends Model
{
    use HasFactory;

    protected $casts = [
        'products_stocking' => 'array'
    ];

    public function scopeFilter($query, array $filters)
    {
        $query
        ->when($filters['name'] ?? null, function ($query, $name) {
            $query->where('name', 'LIKE', "%$name%");
        })->when($filters['region'] ?? null, function ($query, $region) {
            $query->where('region', 'LIKE', "%$region%");
        })->when($filters['type'] ?? null, function ($query, $type) {
            $query->where('type', 'LIKE', "%$type%");
        })->when($filters['town'] ?? null, function ($query, $town) {
            $query->where('town', 'LIKE', "%$town%");
        })->when($filters['location'] ?? null, function ($query, $location) {
            $query->where('location', 'LIKE', "%$location%");
        })->when($filters['suburb'] ?? null, function ($query, $suburb) {
            $query->where('suburb', 'LIKE', "%$suburb%");
        });
    }

    public function engagements()
    {
        return $this->hasMany(ClientEngagement::class, 'sales_outlet_id');
    }

    public function purchaseOrders()
    {
        return $this->hasMany(PurchaseOrder::class, 'sales_outlet_id');
    }

    public function purchaseRequests()
    {
        return $this->hasMany(PurchaseRequest::class, 'sales_outlet_id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function surveys()
    {
        return $this->hasMany(ProductSurvey::class);
    }
}
