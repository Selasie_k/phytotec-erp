<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'department',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'notifications' => 'array',
    ];

    protected $appends = ['role'];

    public function routeNotificationForMNotify($notifiable)
    {
        return $this->phone;
    }

    public function healthFacilities()
    {
        return $this->hasMany(HealthFacility::class);
    }

    public function medicalProfessionals()
    {
        return $this->hasMany(MedicalProfessional::class);
    }

    public function surveys()
    {
        return $this->hasMany(ProductSurvey::class);
    }

    public static function shouldBeNotifiedOfEvent($event)
    {
        return self::all()->filter(function ($user) use ($event) {
            return collect($user->notifications)->contains($event);
        });
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getAllPermissions()
    {
        return $this
                ->roles->load('permissions')
                ->map(fn ($role) => $role->permissions)
                ->flatten()->pluck('name')->unique();
    }

    public function getRoleAttribute()
    {
        return $this->getRoleNames()->first();
    }

    public function clientEngagements()
    {
        return $this->hasMany(ClientEngagement::class);
    }

    public function purchaseOrders()
    {
        return $this->hasMany(PurchaseOrder::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function samples()
    {
        return $this->hasMany(SampleDistribution::class);
    }
    
    public static function hasPermission($permission)
    {
        return static::all(['id', 'name'])
                // ->get(['id', 'name'])
                ->filter(fn ($user) => $user->hasPermissionTo($permission))
                ->filter(fn ($user) => ! in_array($user->id, [1,21, 24, 23])) // remove Paul and Selasie, Hermon, Precious
                ->values();
    }
}
