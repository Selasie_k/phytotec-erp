<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SampleDistribution extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeFilter($query, array $filters)
    {
        $query
        ->when($filters['created_at'] ?? null, function ($query, $created_at) {
            $query->whereBetween('created_at', [$created_at[0], $created_at[1] ?? now()]);
        })->when($filters['receiver_name'] ?? null, function ($query, $receiver_name) {
            $query->where('receiver_name', 'LIKE', "%$receiver_name%");
        })->when($filters['receiver_type'] ?? null, function ($query, $receiver_type) {
            $query->where('receiver_type', 'LIKE', "%$receiver_type%");
        })->when($filters['user_id'] ?? null, function ($query, $user_id) {
            $query->where('user_id', 'LIKE', "%$user_id%");
        })->when($filters['product'] ?? null, function ($query, $product) {
            $query->where('product', 'LIKE', "%$product%");
        })->when($filters['product_type'] ?? null, function ($query, $product_type) {
            $query->where('product_type', 'LIKE', "%$product_type%");
        });
    }
}
