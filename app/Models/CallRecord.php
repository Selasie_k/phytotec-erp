<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CallRecord extends Model
{
    use HasFactory;

    public function scopeFilter($query, array $filters)
    {
        $query
        ->when($filters['date'] ?? null, function ($query, $date) {
            return ! isset($date['end'])
            ? $query->whereDate('created_at', '=', $date['start'])
            : $query->whereDate('created_at', '>=', $date['start'])->whereDate('created_at', '<=', $date['end']);
        })->when($filters['name'] ?? null, function ($query, $name) {
            $query->where('name', 'LIKE', "%$name%");
        })->when($filters['product'] ?? null, function ($query, $product) {
            $query->where('product', 'LIKE', "%$product%");
        })->when($filters['reason'] ?? null, function ($query, $reason) {
            $query->where('reason', 'LIKE', "%$reason%");
        })->when($filters['source'] ?? null, function ($query, $source) {
            $query->where('source', 'LIKE', "%$source%");
        })->when($filters['country'] ?? null, function ($query, $country) {
            $query->where('country', 'LIKE', "%$country%");
        })->when($filters['region'] ?? null, function ($query, $region) {
            $query->where('region', 'LIKE', "%$region%");
        })->when($filters['town'] ?? null, function ($query, $town) {
            $query->where('town', 'LIKE', "%$town%");
        });
    }
}
