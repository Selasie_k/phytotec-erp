<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HealthFacility extends Model
{
    use HasFactory;

    public function medicalProfessionals()
    {
        return $this->belongsToMany(MedicalProfessional::class, 'facility_professional');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeFilter($query, array $filters)
    {
        $query
        ->when($filters['name'] ?? null, function ($query, $name) {
            $query->where('name', 'LIKE', "%$name%");
        })->when($filters['type'] ?? null, function ($query, $type) {
            $query->where('type', 'LIKE', "%$type%");
        })->when($filters['region'] ?? null, function ($query, $region) {
            $query->where('region', 'LIKE', "%$region%");
        })->when($filters['user_id'] ?? null, function ($query, $user_id) {
            $query->where('user_id', $user_id);
        })->when($filters['created_at'] ?? null, function ($query, $created_at) {
            $query->whereBetween('created_at', [$created_at[0], $created_at[1] ?? now()]);
        });
    }
}
