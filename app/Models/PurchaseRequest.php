<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseRequest extends Model
{
    use HasFactory;

    public function outlet()
    {
        return $this->belongsTo(SalesOutlet::class, 'sales_outlet_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeFilter($query, array $filters)
    {
        $query
        ->when($filters['dates'] ?? null, function ($query, $dates) {
            $query->whereBetween('created_at', [$dates[0], $dates[1] ?? now()]);
        })->when($filters['product'] ?? null, function ($query, $product) {
            $query->where('product', 'LIKE', "%$product%");
        });
    }
}
