<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;

    public function totalSalesByExec()
    {
        // $execs = 
        return User::hasPermission('purchase-orders:create')->transform(function($user){
            return [
                'id' => $user->id,
                'name' => $user->name,
                'sales' => [
                    'prostat60' => $user->purchaseOrders()->with('lineItems')->where('product', 'Prostat60')->get()
                ]
            ];
        });
    }
}
