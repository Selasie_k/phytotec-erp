<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductSurvey extends Model
{
    use HasFactory;

    public function outlet()
    {
        return $this->belongsTo(SalesOutlet::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
