<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientEngagement extends Model
{
    use HasFactory;

    protected $casts = [
        'promotional_items' => 'array',
        'products_sold' => 'array',
        'customer_feedback' => 'array',
    ];

    // protected $with = ['outlet'];

    public function outlet()
    {
        return $this->belongsTo(SalesOutlet::class, 'sales_outlet_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['created_at'] ?? null, function ($query, $created_at) {
            $query->whereBetween('created_at', [$created_at[0], $created_at[1] ?? now()]);
        });
    }
}
