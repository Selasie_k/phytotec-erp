<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    use HasFactory;

    protected $appends = ['payment_status', 'payment_due_by'];

    public function outlet()
    {
        return $this->belongsTo(SalesOutlet::class, 'sales_outlet_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function lineItems()
    {
        return $this->hasMany(LineItem::class);
    }

    public function getPaymentDueByAttribute()
    {
        if($this->payment_term == 'Cash'){
            return null;
        }

        return $this->created_at->addDays($this->payment_term);
    }

    public function getPaymentStatusAttribute()
    {
        if($this->payment_term == 'Cash'){
            return 'Settled';
        }

        if(!$this->payment_settled_at){
            if(now()->gt($this->payment_due_by)){
                return 'Overdue';
            }
            return 'Pending';
        }
        
        return 'Settled';
    }

    public function scopeFilter($query, array $filters)
    {
        $query
        ->when($filters['created_at'] ?? null, function ($query, $created_at) {
            $query->whereBetween('created_at', [$created_at[0], $created_at[1] ?? now()]);
        })->when($filters['payment_due_by'] ?? null, function ($query, $payment_due_by) {
            $query->whereBetween('payment_due_by', [$payment_due_by[0], $payment_due_by[1] ?? now()]);
        })->when($filters['check_matures_at'] ?? null, function ($query, $check_matures_at) {
            $query->whereBetween('check_matures_at', [$check_matures_at[0], $check_matures_at[1] ?? now()]);
        })->when($filters['payment_method'] ?? null, function ($query, $payment_method) {
            $query->where('payment_method', 'LIKE', "%$payment_method%");
        })->when($filters['product'] ?? null, function ($query, $product) {
            $query->where('product', 'LIKE', "%$product%");
        })->when($filters['payment_status'] ?? null, function ($query, $payment_status) {
            $query->where('payment_status', 'LIKE', "%$payment_status%");
        });
    }
}
