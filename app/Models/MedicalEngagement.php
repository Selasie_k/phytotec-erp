<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicalEngagement extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function professional()
    {
        return $this->belongsTo(MedicalProfessional::class);
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['daterange'] ?? null, function($query, $daterange){
            $query->whereBetween('created_at', [$daterange[0], $daterange[1] ?? now()]);
        })->when($filters['product'] ?? null, function ($query, $product) {
            $query->where('product', 'LIKE', "%$product%");
        })->when($filters['purpose'] ?? null, function ($query, $purpose) {
            $query->where('purpose', 'LIKE', "%$purpose%");
        })->when($filters['user_id'] ?? null, function ($query, $user_id) {
            $query->where('user_id', $user_id);
        })->when($filters['created_at'] ?? null, function ($query, $created_at) {
            $query->whereBetween('created_at', [$created_at[0], $created_at[1] ?? now()]);
        });
    }
}
