<?php

namespace App\Http\Controllers;

use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Models\MedicalEngagement;
use App\Models\MedicalProfessional;

class MedicalEngagementController extends Controller
{
    const EngagementPurposes = [ 'Presentation', 'Follow-up' ];

    public function index(Request $request)
    {
        $query = MedicalEngagement::query();

        if(!auth()->user()->hasPermissionTo('medical-engagements:view-all')){
            $query->where('user_id', auth()->id());
        }

        return Inertia::render('Medical/Engagements/Index', [
            'engagements' => $query->with('user:id,name')->filter($request->all())->latest()->paginate(20),
            'products' => config('phytotec.products'),
            'purposes' => static::EngagementPurposes,
            'users' => User::hasPermission('medical-engagements:create'),
        ]);
    }

    public function create(MedicalProfessional $professional)
    {
        return $this->edit(new MedicalEngagement, $professional);
    }

    public function edit(MedicalEngagement $engagement, MedicalProfessional $professional = null)
    {
        return Inertia::render('Medical/Engagements/Form', [
            'engagement' => $engagement->exists ? $engagement : (object) [],
            'professional' => $professional ?? $engagement->professional,
            'products' => config('phytotec.products'),
            'purposes' => static::EngagementPurposes,
        ]);
    }

    public function store(Request $request)
    {
        MedicalEngagement::create($request->validate([
            'professional_id' => 'required',
            'name' => 'required',
            'purpose' => 'required',
            'product' => 'required',
            'user_id' => 'required',
            // 'samples_issued' => 'nullable',
            // 'individuals_screened' => 'nullable',
            // 'subscriptions_issued' => 'nullable',
            'notes' => 'nullable',
        ]));

        return redirect()->route('medical.engagements')->with('success', 'Medical engagement saved successfully');
    }

    public function update(Request $request, MedicalEngagement $engagement)
    {
        $engagement->update($request->validate([
            'name' => 'required',
            'purpose' => 'required',
            'product' => 'required',
            // 'samples_issued' => 'nullable',
            // 'individuals_screened' => 'nullable',
            // 'subscriptions_issued' => 'nullable',
            'notes' => 'nullable',
        ]));

        return redirect()->route('medical.engagements')->with('success', 'Medical engagement updated successfully');
    }

    public function destroy(MedicalEngagement $engagement)
    {
        $engagement->delete();
        return redirect()->route('medical.engagements')->with('success', 'Medical engagement deleted');
    }
}
