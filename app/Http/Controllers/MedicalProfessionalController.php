<?php

namespace App\Http\Controllers;

use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Models\HealthFacility;
use App\Models\MedicalProfessional;

class MedicalProfessionalController extends Controller
{
    public function index(Request $request)
    {
        return Inertia::render('Medical/Professionals/Index', [
            'professionals' => MedicalProfessional::with('health_facility:id,name', 'user:id,name')->filter($request->all())->latest()->paginate(20),
            'products' => collect(config('phytotec.products'))->push('Not Prescribing') ,
            'specialties' => config('phytotec.medical.specialties'),
            'users' => User::hasPermission('medical-professionals:create'),
        ]);
    }

    public function create()
    {
        return $this->edit(new MedicalProfessional);
    }

    public function edit(MedicalProfessional $professional)
    {
        return Inertia::render('Medical/Professionals/Form', [
            'professional' => $professional->exists ? $professional->load('healthFacilities:id,name') : (object) [],
            'products' => config('phytotec.products'),
            'facilities' => HealthFacility::select(['id', 'name'])->get(),
            'specialties' => config('phytotec.medical.specialties'),
        ]);
    }

    public function store(Request $request)
    {
        $professional = auth()->user()->medicalProfessionals()->create($request->validate([
            // 'health_facility_id' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'email' => 'nullable',
            'specialty' => 'required',
            'products_prescribed' => 'array',
        ]));

        // return redirect()->route('medical.professionals.edit', )->with('success', 'Medical Professional saved successfully');
        return $this->edit($professional);
    }

    public function update(Request $request, MedicalProfessional $professional)
    {
        $professional->update($request->validate([
            'health_facility_id' => 'nullable',
            'name' => 'required',
            'phone' => 'required',
            'email' => 'nullable',
            'specialty' => 'required',
            'products_prescribed' => 'array',
        ]));

        return redirect()->back()->with('success', 'Medical Professional updated successfully');
    }

    public function destroy(MedicalProfessional $professional)
    {
        $professional->delete();
        return redirect()->route('medical.professionals')->with('success', 'Medical Professional deleted successfully');
    }
}
