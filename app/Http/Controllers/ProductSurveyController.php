<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\SalesOutlet;
use Illuminate\Http\Request;
use App\Models\ProductSurvey;

class ProductSurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->edit(new ProductSurvey);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        // auth()->user()->surveys()->create($request->validate([
        //     'product' => $request->product,
        //     'outlet_id' => $request->outlet_id,
        //     'has_product' => $request->has_product,
        //     'quantity' => $request->quantity,
        //     'price' => $request->price,
        //     'remarks' => $request->remarks,
        // ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductSurvey  $productSurvey
     * @return \Illuminate\Http\Response
     */
    public function show(ProductSurvey $productSurvey)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductSurvey  $productSurvey
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductSurvey $productSurvey)
    {
        return Inertia::render('Marketing/Survey/Form', [
            'survey' => $productSurvey->exists ? $productSurvey : (object) [],
            'outlet' => SalesOutlet::find(request('outlet_id')),
            'product' => request('product'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductSurvey  $productSurvey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductSurvey $productSurvey)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductSurvey  $productSurvey
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductSurvey $productSurvey)
    {
        //
    }
}
