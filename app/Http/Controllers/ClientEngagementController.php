<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Region;
use Illuminate\Http\Request;
use App\Models\ClientEngagement;
use App\Models\SalesOutlet;

class ClientEngagementController extends Controller
{
    const ReasonsForNotSelling = [
        'Low demand',
        'Ignorance of products',
        'High price'
    ];

    const PromotionalItems = [
        'Posters',
        'Pull-Up Banner',
        'Fliers',
        'T-Shirts',
    ];

    const ProductVisibility = [
        'Very visible',
        'Semi-visible',
        'Hidden'
    ];

    const PurchaseModes = [
        'With subscription',
        'Without subscription',
    ];

    const CustomerFeedbacks = [
        'Product did not give desired results',
        'Product takes time to have desired results',
        'Product used to give desired results until recently ',
        'Product exacerbated my presenting complaints (complained symptoms)',
        'Product subsided my presenting complaints (complained symptoms)',
        'Product gave desired results/worked for me',
        'Product worked for me when taken with other prescribed orthodox medications',
        'Product is good and I have recommended it to family & friends',
    ];

    public function index(Request $request)
    {
        $query = ClientEngagement::query();
        if(!auth()->user()->hasPermissionTo('engagements:view-all')){
            $query->where('user_id', auth()->id());
        }

        return Inertia::render('Marketing/Engagements/Index',[
            'engagements' => $query->with('outlet', 'user')
                            ->filter($request->all())
                            ->latest()
                            ->paginate(20)
        ]);
    }

    public function create(SalesOutlet $outlet)
    {
        return $this->edit(new ClientEngagement, $outlet);
    }

    public function edit(ClientEngagement $engagement, SalesOutlet $outlet = null)
    {
        return Inertia::render('Marketing/Engagements/Form', [
            'engagement' => $engagement->exists ? $engagement->load('user') : (object) [],
            'outlet' => $outlet ?? $engagement->outlet,
            'reasons_for_not_selling' => self::ReasonsForNotSelling,
            'promotional_items' => self::PromotionalItems,
            'product_visibility' => self::ProductVisibility,
            'purchase_modes' => self::PurchaseModes,
            'customer_feedbacks' => self::CustomerFeedbacks,
            'products' => config('phytotec.products'),
        ]);
    }

    public function store(Request $request)
    {
        ClientEngagement::create($request->validate([
            'user_id' => 'required',
            'sales_outlet_id' => 'required',
            'sells_products' => 'nullable',
            'products_sold' => 'nullable',
            'reason_for_not_selling' => 'nullable',
            'promotional_items' => 'nullable',
            'product_visibility' => 'nullable',
            'supplier_type' => 'nullable',
            'supplier_name' => 'nullable',
            'estimated_sales_volume' => 'nullable',
            'customer_purchase_mode' => 'nullable',
            'customer_feedback' => 'nullable',
            'recommends_products' => 'nullable',
            'remarks' => 'nullable',
        ]));
        return redirect()->route('marketing.engagements')->with('success', 'Client engagement record saved');
    }

    public function update(Request $request, ClientEngagement $engagement)
    {
        $engagement->update($request->validate([
            'sells_products' => 'nullable',
            'products_sold' => 'nullable',
            'reason_for_not_selling' => 'nullable',
            'promotional_items' => 'nullable',
            'product_visibility' => 'nullable',
            'supplier_type' => 'nullable',
            'supplier_name' => 'nullable',
            'estimated_sales_volume' => 'nullable',
            'customer_purchase_mode' => 'nullable',
            'customer_feedback' => 'nullable',
            'recommends_products' => 'nullable',
            'remarks' => 'nullable',
        ]));
        return redirect()->back()->with('success', 'Record Updated');
    }

    public function destroy(ClientEngagement $engagement)
    {
        $engagement->delete();
        return redirect()->route('marketing.engagements')->with('success', 'Record deleted!');
    }
}
