<?php

namespace App\Http\Controllers;

use App\Models\PurchaseOrder;
use Illuminate\Http\Request;

class ConfirmedPurchaseOrderController extends Controller
{
    public function store(PurchaseOrder $po)
    {
        $po->update(['confirmed_at' => now()]);

        // fire notification here
        return redirect()->back()->with('success', 'Purchase order confirmed!');
    }
}
