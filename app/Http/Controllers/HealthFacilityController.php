<?php

namespace App\Http\Controllers;

use App\Models\User;
use Inertia\Inertia;
use App\Models\Region;
use Illuminate\Http\Request;
use App\Models\HealthFacility;

class HealthFacilityController extends Controller
{

    const FacilityTypes = [
        'Hospital',
        'Herbal Clinic',
        'Pharmacy',
    ];

    public function index(Request $request)
    {
        return Inertia::render('Medical/Facilities/Index', [
            'facilities' => HealthFacility::latest()->with('user:id,name')->filter($request->all())->paginate(20),
            'facility_types' => static::FacilityTypes,
            'regions' => Region::all()->pluck('name'),
            'users' => User::hasPermission('health-facilities:create'),
        ]);
    }

    public function create()
    {
        return $this->edit(new HealthFacility);
    }

    public function edit(HealthFacility $facility)
    {
        return Inertia::render('Medical/Facilities/Form', [
            'facility' => $facility->exists ? $facility->load('medicalProfessionals:id,name') : (object) [],
            'facility_types' => static::FacilityTypes,
            'regions' => Region::all()->pluck('name'),
        ]);
    }

    public function store(Request $request)
    {
       
        auth()->user()->healthFacilities()->create($request->validate([
            'name' => 'required',
            'type' => 'required',
            'region' => 'required',
            'town' => 'required',
            'digital_address' => 'nullable',
        ]));

        return redirect()->route('medical.facilities')->with('success', 'Health Facility added successfully');
    }

    public function update(Request $request, HealthFacility $facility)
    {
        $facility->update($request->validate([
            'name' => 'required',
            'type' => 'required',
            'region' => 'required',
            'town' => 'required',
            'digital_address' => 'nullable'
        ]));

        return redirect()->route('medical.facilities')->with('success', 'Health Facility updated successfully');
    }

    public function destroy(HealthFacility $facility)
    {
        $facility->delete();
        return redirect()->route('medical.facilities')->with('success', 'Health Facility deleted successfully');
    }
}
