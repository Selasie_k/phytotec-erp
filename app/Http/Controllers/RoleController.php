<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Spatie\Permission\Exceptions\RoleAlreadyExists;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Admins/ACL/Index', [
            'roles' => Role::all()
                        ->transform(function($role){
                            return [
                                'id' => $role->id,
                                'name' => $role->name,
                                'permissions' => $role->permissions->pluck('name')
                            ];
                        }),
            'permissions' => Permission::all()
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $role = Role::findOrCreate($request->name);

        $role->syncPermissions($request->permissions);
        
        return redirect()->back()->with('success', 'Role Saved');
    }

    // public function update(Request $request, Role $role)
    // {
    //     $role->update($request->validate([
    //         'name' => 'required|unique:roles'
    //     ]));
        
    //     return redirect()->back()->with('success', 'Role updated');     
    // }

    public function destroy(Role $role)
    {
        $role->delete();
        return redirect()->back()->with('success', 'Role deleted');
    }
}
