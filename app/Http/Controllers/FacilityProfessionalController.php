<?php

namespace App\Http\Controllers;

use App\Models\HealthFacility;
use App\Models\MedicalProfessional;
use Illuminate\Http\Request;

class FacilityProfessionalController extends Controller
{
    public function store(MedicalProfessional $professional)
    {
        $professional->healthFacilities()->syncWithoutDetaching(request('facility_id'));

        return redirect()->back();
    }

    public function destroy(MedicalProfessional $professional)
    {
        $professional->healthFacilities()->detach(request('facility_id'));
        return redirect()->back();
    }
}
