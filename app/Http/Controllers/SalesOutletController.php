<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Region;
use App\Models\Product;
use App\Models\CallReason;
use App\Models\SalesOutlet;
use Illuminate\Http\Request;

class SalesOutletController extends Controller
{
    protected $OUTLET_TYPES = [
        'Retail',
        'Wholesale',
        'Medical Facility'
    ];

    public function index(Request $request)
    {
        return Inertia::render('SalesOutlets/Index', [
            'salesOutlets' => SalesOutlet::latest()->filter($request->all())->paginate(15),
            'regions' => Region::all()->pluck('name'),
            'products' => config('phytotec.products'),
            'outlet_types' => $this->OUTLET_TYPES
        ]);
    }

    public function create()
    {
        return $this->edit(new SalesOutlet);
    }

    public function store(Request $request)
    {
        SalesOutlet::create($request->validate([
            'name' => 'required',
            'type' => 'required',
            'wholesale_type' => 'nullable',
            'region' => 'required',
            'town' => 'required',
            'location' => 'required',
            'digital_address' => 'nullable',
            'contact_person' => 'required',
            'phone' => 'required',
            'products_stocking' => 'nullable|array'
        ]));

        return redirect()->route('outlets')->with('success', 'Outlet Created!');
    }

    public function edit(SalesOutlet $outlet)
    {
        return Inertia::render('SalesOutlets/Form', [
            'outlet' => $outlet->exists ? $outlet : (object) [],
            'regions' => Region::all()->pluck('name'),
            'products' => config('phytotec.products'),
            'outlet_types' => $this->OUTLET_TYPES
        ]);
    }

    public function update(Request $request, SalesOutlet $outlet)
    {
        $outlet->update($request->validate([
            'name' => 'required',
            'type' => 'required',
            'wholesale_type' => 'nullable',
            'region' => 'required',
            'town' => 'required',
            'location' => 'required',
            'digital_address' => 'nullable',
            'contact_person' => 'required',
            'phone' => 'required',
            'products_stocking' => 'nullable|array'
        ]));

        return redirect()->route('outlets')->with('success', 'Outlet Updated');
    }

    public function destroy(SalesOutlet $outlet)
    {
        $outlet->delete();
        return redirect()->route('outlets')->with('success', 'Outlet deleted');
    }
}
