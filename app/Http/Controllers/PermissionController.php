<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PermissionController extends Controller
{

    public function index()
    {
        //
    }

    public function create(Request $request)
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // delete permissions
        // Permission::all()->each->delete();

        // create permissions
        // calls
        Permission::findOrCreate('calls:view-all');
        Permission::findOrCreate('calls:view');
        Permission::findOrCreate('calls:show');
        Permission::findOrCreate('calls:create');
        Permission::findOrCreate('calls:update');
        Permission::findOrCreate('calls:delete');

        // outlets
        Permission::findOrCreate('outlets:view-all');
        Permission::findOrCreate('outlets:view');
        Permission::findOrCreate('outlets:show');
        Permission::findOrCreate('outlets:create');
        Permission::findOrCreate('outlets:update');
        Permission::findOrCreate('outlets:delete');

        // engagements
        Permission::findOrCreate('engagements:view-all');
        Permission::findOrCreate('engagements:view');
        Permission::findOrCreate('engagements:show');
        Permission::findOrCreate('engagements:create');
        Permission::findOrCreate('engagements:update');
        Permission::findOrCreate('engagements:delete');

        // purchase orders
        Permission::findOrCreate('purchase-orders:view-all');
        Permission::findOrCreate('purchase-orders:view');
        Permission::findOrCreate('purchase-orders:show');
        Permission::findOrCreate('purchase-orders:create');
        Permission::findOrCreate('purchase-orders:update');
        Permission::findOrCreate('purchase-orders:delete');
        Permission::findOrCreate('purchase-orders:confirm');

        // purchase requests
        Permission::findOrCreate('purchase-requests:view-all');
        Permission::findOrCreate('purchase-requests:view');
        Permission::findOrCreate('purchase-requests:show');
        Permission::findOrCreate('purchase-requests:create');
        Permission::findOrCreate('purchase-requests:update');
        Permission::findOrCreate('purchase-requests:delete');
        Permission::findOrCreate('purchase-requests:confirm');

        // health facilities
        Permission::findOrCreate('health-facilities:view-all');
        Permission::findOrCreate('health-facilities:view');
        Permission::findOrCreate('health-facilities:show');
        Permission::findOrCreate('health-facilities:create');
        Permission::findOrCreate('health-facilities:update');
        Permission::findOrCreate('health-facilities:delete');

        // medical professional
        Permission::findOrCreate('medical-professionals:view-all');
        Permission::findOrCreate('medical-professionals:view');
        Permission::findOrCreate('medical-professionals:show');
        Permission::findOrCreate('medical-professionals:create');
        Permission::findOrCreate('medical-professionals:update');
        Permission::findOrCreate('medical-professionals:delete');

        // medical engagement
        Permission::findOrCreate('medical-engagements:view-all');
        Permission::findOrCreate('medical-engagements:view');
        Permission::findOrCreate('medical-engagements:show');
        Permission::findOrCreate('medical-engagements:create');
        Permission::findOrCreate('medical-engagements:update');
        Permission::findOrCreate('medical-engagements:delete');

        // payments
        Permission::findOrCreate('payments:view-all');
        Permission::findOrCreate('payments:view');
        Permission::findOrCreate('payments:show');
        Permission::findOrCreate('payments:create');
        Permission::findOrCreate('payments:update');
        Permission::findOrCreate('payments:delete');

        // samples
        Permission::findOrCreate('samples:view-all');
        Permission::findOrCreate('samples:view');
        Permission::findOrCreate('samples:show');
        Permission::findOrCreate('samples:create');
        Permission::findOrCreate('samples:update');
        Permission::findOrCreate('samples:delete');

        // reports
        Permission::findOrCreate('reports:view-all');

        return redirect()->back()->with('success', 'Permissions seeded');
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Permission $permission)
    {
        //
    }

    public function update(Request $request, Permission $permission)
    {
        //
    }

    public function destroy(Permission $permission)
    {
        //
    }
}
