<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\SalesOutlet;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PaymentController extends Controller
{
    public function index()
    {
        $query = Payment::query();
        if(!auth()->user()->hasPermissionTo('payments:view-all')){
            $query->where('user_id', auth()->id());
        }

        return Inertia::render('Sales/Payments/Index', [
            'payments' => $query->latest()->with('user', 'outlet')->paginate(20),
        ]);
    }

    public function create(SalesOutlet $outlet)
    {
        return $this->edit(new Payment, $outlet);
    }

    public function store(Request $request)
    {
        Payment::create($request->validate([
            'amount' => 'required',
            'method' => 'required',
            'check_number' => 'nullable',
            'check_due_date' => $request->filled('check_number') ? 'required' : 'nullable',
            'user_id' => 'required',
            'outlet_id' => 'required',
        ]));

        return redirect()->back()->with('success', 'Payment record created!');
    }

    public function edit(Payment $payment, SalesOutlet $outlet = null)
    {
        return Inertia::render('Sales/Payments/Form', [
            'payment' => $payment->exists ? $payment->load('outlet') : (object) [],
            'outlet' => $outlet ?? $payment->outlet
        ]);
    }

    public function update(Request $request, Payment $payment)
    {
        $payment->update($request->validate([
            'amount' => 'required',
            'method' => 'required',
            'check_number' => 'nullable',
            'check_due_date' => $request->filled('check_number') ? 'required' : 'nullable',
        ]));

        return redirect()->back()->with('success', 'Payment record updated!');
    }

    public function destroy(Payment $payment)
    {
        $payment->delete();
        return redirect()->route('sales.payments')->with('success', 'Payment record deleted!');
    }
}
