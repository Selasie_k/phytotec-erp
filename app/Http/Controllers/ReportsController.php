<?php

namespace App\Http\Controllers;

use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    public function sales(Request $request)
    {
        $filters = request()->all();

        return Inertia::render('Reports/Sales', [
            'users' => User::hasPermission('purchase-orders:create')->transform(function($user) use ($filters){

                $pos = $user->purchaseOrders()->with('lineItems')->filter($filters)->get();
                // $pos = $user->purchaseOrders()->with('lineItems')->when($filters['created_at'] ?? null, function ($query, $created_at) {
                //     $query->whereBetween('created_at', [$created_at[0], $created_at[1] ?? now()]);
                // })->get();

                $lineItems = $pos->map(fn($po) => $po->lineItems)->flatten();

                return [
                    'id' => $user->id,
                    'name' => $user->name,
                    'purchaseOrders' => [
                        'Prostat60' => [
                            'Pack of 10' => $lineItems->where('product', 'Prostat60')->where('product_type', 'Pack of 10')->sum('quantity'),
                            'Pack of 30' => $lineItems->where('product', 'Prostat60')->where('product_type', 'Pack of 30')->sum('quantity'),
                            'Total' => $lineItems->where('product', 'Prostat60')->sum('quantity'),
                        ],
                        'Demaphyte' => [
                            'Bottles' => $lineItems->where('product', 'Demaphyte')->sum('quantity'),
                            'Total' => $lineItems->where('product', 'Demaphyte')->sum('quantity'),
                        ],
                        'Omega-Zee' => [
                            'Pack of 10' => $lineItems->where('product', 'Omega-Zee')->sum('quantity'),
                            'Total' => $lineItems->where('product', 'Omega-Zee')->sum('quantity'),
                        ],
                        'ZetaPRO' => [
                            'Pack of 10' => $lineItems->where('product', 'ZetaPRO')->sum('quantity'),
                            'Total' => $lineItems->where('product', 'ZetaPRO')->sum('quantity'),
                        ],
                    ],
                    'payments' => $user->payments()->filter($filters)->sum('amount')
                ];
            })
        ]);
    }

    public function samples(Request $request)
    {
        $filters = request()->all();

        return Inertia::render('Reports/Samples', [
            'users' => User::hasPermission('samples:create')->transform(function($user) use ($filters){

                $samples = $user->samples()->filter($filters)->get();

                return [
                    'id' => $user->id,
                    'name' => $user->name,
                    'purchaseOrders' => [
                        'Prostat60' => [
                            'Pack of 10' => $samples->where('product', 'Prostat60')->where('product_type', 'Pack of 10')->sum('quantity'),
                            'Pack of 30' => $samples->where('product', 'Prostat60')->where('product_type', 'Pack of 30')->sum('quantity'),
                            'Total' => $samples->where('product', 'Prostat60')->sum('quantity'),
                        ],
                        'Demaphyte' => [
                            'Bottles' => $samples->where('product', 'Demaphyte')->sum('quantity'),
                            'Total' => $samples->where('product', 'Demaphyte')->sum('quantity'),
                        ],
                        'Omega-Zee' => [
                            'Pack of 10' => $samples->where('product', 'Omega-Zee')->sum('quantity'),
                            'Total' => $samples->where('product', 'Omega-Zee')->sum('quantity'),
                        ],
                        'ZetaPRO' => [
                            'Pack of 10' => $samples->where('product', 'ZetaPRO')->sum('quantity'),
                            'Total' => $samples->where('product', 'ZetaPRO')->sum('quantity'),
                        ],
                    ],
                ];
            })
        ]);
    }

    public function marketing(Request $request)
    {
        $filters = request()->all();

        return Inertia::render('Reports/Marketing', [
            'users' => User::hasPermission('engagements:create')->transform(function($user) use ($filters){

                $engagements = $user->clientEngagements()->with('outlet')->filter($filters)->get();

                // dump($engagements->where('products_sold', "Prostat60"));

                return [
                    'id' => $user->id,
                    'name' => $user->name,
                    'totalEngagements' => $engagements->count(),
                    // 'totalRetails' => $engagements->count() ? $engagements->filter(fn($eng) => $eng->outlet->type == 'Retail')->count() : 0,
                    'totalRetails' => $engagements->filter(fn($eng) => optional($eng->outlet)->type == 'Retail')->count(),
                    'totalWholesales' => $engagements->filter(fn($eng) => optional($eng->outlet)->type == 'Wholesale')->count(),
                    // 'totalWholesales' => $engagements->whereHas('outlet', fn($query) => $query->where('type', 'Wholesale'))->count(),
                    // 'totalWholesales' => $engagements->whereHas('outlet', fn($query) => $query->where('type', 'Wholesale'))->filter(fn($eng) => $eng->outlet->type == 'Wholesale')->count(),
                    // 'totalWithPromoItems' => $engagements->filter(fn($eng) => collect($eng->promotional_items)->count() > 0 )->count(),
                    'totalWithPromoItems' => $engagements->filter(fn($eng) => count($eng->promotional_items) > 0 )->count(),
                    'visibilities' => [
                        'Prostat60' => [
                            'Hidden' => $engagements->where('products_sold', "Prostat60")->where('product_visibility', 'Hidden')->count(),
                            'Semi' => $engagements->where('products_sold', "Prostat60")->where('product_visibility', 'Semi-visible')->count(),
                            'High' => $engagements->where('products_sold', "Prostat60")->where('product_visibility', 'Very Visible')->count(),
                        ],
                        'Demaphyte' => [
                            'Hidden' => $engagements->where('products_sold', "Demaphyte")->where('product_visibility', 'Hidden')->count(),
                            'Semi' => $engagements->where('products_sold', "Demaphyte")->where('product_visibility', 'Semi-visible')->count(),
                            'High' => $engagements->where('products_sold', "Demaphyte")->where('product_visibility', 'Very Visible')->count(),
                        ]
                    ]
                ];
            })
        ]);
    }
}