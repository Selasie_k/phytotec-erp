<?php

namespace App\Http\Controllers;

use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Models\SampleDistribution;

class SampleDistributionController extends Controller
{
    const ReceiverTypes = ['Outlet', 'Doctor', 'Organization', 'Individual'];

    public function index()
    {
        $query = SampleDistribution::query();
        if(!auth()->user()->hasPermissionTo('samples:view-all')){
            $query->where('user_id', auth()->id());
        }
        
        return Inertia::render('Samples/Index', [
            'samples' => $query->filter(request()->all())->with('user:id,name')->latest()->paginate(20),
            'receiver_types' => static::ReceiverTypes,
            'products' => config('phytotec.products'),
            'product_types' => config('phytotec.product_types'),
            'issuers' => User::Permission('samples:create')
                            ->get(['id','name'])
                            ->filter(fn($user) => ! in_array($user->id, [1,21]))
                            ->values()
        ]);
    }

    public function create(Request $request)
    {
        return $this->edit(
            (new SampleDistribution)->fill($request->all())
        );
    }

    public function edit(SampleDistribution $sample)
    {
        return Inertia::render('Samples/Form', [
            'sample' => (object) $sample->getAttributes(),
            'products' => config('phytotec.products'),
            'product_types' => config('phytotec.product_types'),
            'receiver_types' => static::ReceiverTypes,
        ]);
    }

    public function store(Request $request)
    {
        SampleDistribution::create($request->validate([
            'user_id' => 'required',
            'receiver_name' => 'required',
            'receiver_type' => 'required',
            'product' => 'required',
            'product_type' => 'required',
            'quantity' => 'required',
        ]));

        return redirect()->route('samples')->with('success', 'Sample distribution recorded!');
    }

    public function update(Request $request, SampleDistribution $sample)
    {
        $sample->update($request->validate([
            'receiver_name' => 'required',
            'receiver_type' => 'required',
            'product' => 'required',
            'product_type' => 'required',
            'quantity' => 'required',
        ]));

        return redirect()->route('samples')->with('success', 'Sample distribution recorded!');
    }

    public function destroy(SampleDistribution $sample)
    {
        $sample->delete();
        return redirect()->route('samples')->with('success', 'Record deleted!');
    }
}
