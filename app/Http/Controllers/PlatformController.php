<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PlatformController extends Controller
{
    public function index()
    {
        return Inertia::render('Admins/Platform/Index', [
            'products' => Product::all()
        ]);
    }
}
