<?php

namespace App\Http\Controllers;

use App\Models\CallReason;
use Illuminate\Http\Request;

class CallReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CallReason  $callReason
     * @return \Illuminate\Http\Response
     */
    public function show(CallReason $callReason)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CallReason  $callReason
     * @return \Illuminate\Http\Response
     */
    public function edit(CallReason $callReason)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CallReason  $callReason
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CallReason $callReason)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CallReason  $callReason
     * @return \Illuminate\Http\Response
     */
    public function destroy(CallReason $callReason)
    {
        //
    }
}
