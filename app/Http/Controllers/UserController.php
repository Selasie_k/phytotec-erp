<?php

namespace App\Http\Controllers;

use App\Events\PurchaseOrderCreated;
use App\Events\PurchaseRequestCreated;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index()
    {
        return Inertia::render('Admins/Users/Index', [
            'users' => User::paginate(20),
        ]);
    }

    public function create()
    {
        return $this->edit(new User);
    }

    public function store(Request $request)
    {
        $user = User::create($request->validate([
            'name' => 'required',
            'email' => 'required',
            'department' => 'required',
            'password' => 'required|min:6'
        ]));

        $user->syncRoles($request->role);
        return redirect()->route('admins.users')->with('success', 'User Created!');
    }

    public function edit(User $user)
    {
        return Inertia::render('Admins/Users/Form', [
            'user' => $user->exists ? $user : (object) [],
            'roles' => Role::all()->pluck('name'),
            'departments' => config('phytotec.departments'),
            'notifications' => [
                PurchaseOrderCreated::class,
                PurchaseRequestCreated::class
            ]
        ]);
    }

    public function update(Request $request, User $user)
    {
        $user->update($request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'department' => 'required',
            'notifications' => 'nullable',
            'password' => 'sometimes|nullable|min:6',
        ]));

        $user->syncRoles($request->role);
        return redirect()->route('admins.users')->with('success', 'User Updated');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('call-records')->with('success', 'User deleted');
    }
}
