<?php

namespace App\Http\Controllers;

use App\Models\AdSource;
use App\Models\CallReason;
use App\Models\CallRecord;
use App\Models\Product;
use App\Models\Region;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CallRecordController extends Controller
{

    public function index(Request $request)
    {
        return Inertia::render('CallRecords/Index', [
            'callRecords' => fn () => CallRecord::latest()->filter($request->all())->paginate(15),
            'reasons' => fn () => CallReason::all()->pluck('name'),
            'regions' => fn () => Region::all()->pluck('name'),
            'products' => fn () => array_merge(config('phytotec.products'), ['Others']),
            'ad_sources' => fn () => AdSource::all()->pluck('name'),
        ]);
    }

    public function create()
    {
        return $this->edit(new CallRecord);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'town' => 'required',
            'source' => 'required',
            'region' => 'required',
        ]);

        CallRecord::create($request->all());
        return redirect()->route('call-records')->with('success', 'Record Created!');
    }

    public function edit(CallRecord $caller)
    {
        return Inertia::render('CallRecords/Form', [
            'record' => $caller->exists ? $caller : (object) [],
            'reasons' => CallReason::all()->pluck('name'),
            'regions' => Region::all()->pluck('name'),
            'products' => array_merge(config('phytotec.products'), ['Others']),
            'ad_sources' => AdSource::all()->pluck('name'),
        ]);
    }

    public function update(Request $request, CallRecord $caller)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'town' => 'required',
            'source' => 'required',
            'region' => 'required',
        ]);

        $caller->update($request->all());
        return redirect()->route('call-records')->with('success', 'Record Updated');
    }

    public function destroy(CallRecord $caller)
    {
        $caller->delete();
        return redirect()->route('call-records')->with('success', 'Record deleted');
    }
}
