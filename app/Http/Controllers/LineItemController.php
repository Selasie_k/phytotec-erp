<?php

namespace App\Http\Controllers;

use App\Models\LineItem;
use App\Models\PurchaseOrder;
use Illuminate\Http\Request;

class LineItemController extends Controller
{

    public function store(Request $request, PurchaseOrder $po)
    {
        $po->lineItems()->create($request->all());
        return redirect()->back()->with('success', 'Line item added!');
    }

    public function destroy(LineItem $li)
    {
        $li->delete();
        return redirect()->back()->with('success', 'Line item removed!');
    }
}
