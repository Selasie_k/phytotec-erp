<?php

namespace App\Http\Controllers;

use App\Models\PurchaseRequest;
use App\Models\SalesOutlet;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PurchaseRequestController extends Controller
{
    public function index()
    {
        $query = PurchaseRequest::query();
        if(!auth()->user()->hasPermissionTo('purchase-requests:view-all')){
            $query->where('user_id', auth()->id());
        }

        return Inertia::render('Marketing/PurchaseRequests/Index', [
            'purchaseRequests' => $query->latest()->filter(request()->all())->with('outlet:id,name', 'user:id,name')->paginate(20),
            'products' => config('phytotec.products')
        ]);
    }

    public function create(Request $request)
    {
        return $this->edit(new PurchaseRequest, $request);
    }

    public function edit(PurchaseRequest $purchaseRequest, Request $request)
    {
        return Inertia::render('Marketing/PurchaseRequests/Form', [
            'purchaseRequest' => $purchaseRequest->exists ? $purchaseRequest : (object) [],
            'outlet' => SalesOutlet::find($request->outlet),
            'products' => config('phytotec.products'),
            'product_types' => config('phytotec.product_types'),
        ]);
    }

    public function store(Request $request)
    {
        PurchaseRequest::create($request->validate([
            'product' => 'required',
            'quantity' => 'required',
            'product_type' => 'required',
            'notes' => 'nullable',
            'sales_outlet_id' => 'required',
            'user_id' => 'required',
        ]));

        return redirect()->route('marketing.purchase-requests')->with('success', 'Purchase request saved!');
    }

    public function update(Request $request, PurchaseRequest $purchaseRequest)
    {
        $purchaseRequest->update($request->validate([
            'product' => 'required',
            'quantity' => 'required',
            'product_type' => 'required',
            'notes' => 'nullable',
        ]));

        return redirect()->back()->with('success', 'Purchase request updated!');
    }

    public function destroy(PurchaseRequest $purchaseRequest)
    {
        $purchaseRequest->delete();
        return redirect()->route('marketing.purchase-requests')->with('success', 'Record deleted!');
    }
}
