<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ImpersonatedUserController extends Controller
{
    public function __invoke(User $user)
    {
        Auth::login($user);

        return redirect('/')->with('success', "Logged in as $user->name");
    }
}
