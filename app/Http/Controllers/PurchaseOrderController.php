<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\SalesOutlet;
use Illuminate\Http\Request;
use App\Models\PurchaseOrder;
use App\Events\PurchaseOrderCreated;

class PurchaseOrderController extends Controller
{
    public function index(Request $request)
    {
        $query = PurchaseOrder::query();
        if(!auth()->user()->hasPermissionTo('purchase-orders:view-all')){
            $query->where('user_id', auth()->id());
        }

        return Inertia::render('Sales/PurchaseOrders/Index', [
            'purchase_orders' => $query
                        ->latest()
                        ->filter($request->all())
                        ->with('user:id,name', 'outlet:id,name')
                        ->withCount('lineItems')
                        ->paginate(20),
            'products' => config('phytotec.products')
        ]);
    }

    public function create(SalesOutlet $outlet)
    {
        return $this->edit(new PurchaseOrder(), $outlet);
    }

    public function store(Request $request)
    {
        $po = PurchaseOrder::create($request->validate([
            'sales_outlet_id' => 'required',
            'user_id' => 'required',
            'payment_term' => 'required',
            'delivery_date' => 'required',
            'remarks' => 'nullable',
        ]));

        $po->lineItems()->createMany($request->line_items);

        // $request->merge(['user_id' => auth()->id()]);

        // $outlet->purchaseOrders()->create($request->all());
        event(new PurchaseOrderCreated($po));

        return redirect()->route('sales.purchase-orders.edit', ['po' => $po, 'outlet' => $po->outlet])->with('success', 'Purchase order created!');
    }

    public function edit(PurchaseOrder $po, SalesOutlet $outlet)
    {
        return Inertia::render('Sales/PurchaseOrders/Form', [
            'purchase_order' => $po->exists ? $po->load(['user', 'lineItems']) : (object) [],
            'outlet' => $outlet,
            'products' => config('phytotec.products'),
            'product_types' => config('phytotec.product_types'),
        ]);
    }

    public function update(Request $request, PurchaseOrder $po)
    {
        $po->update($request->validate([
            'payment_term' => 'required',
            'delivery_date' => 'required',
            'remarks' => 'nullable',
        ]));

        // $po->update($request->except('outlet', 'user'));

        return redirect()->back()->with('success', 'Purchase order updated');
    }

    public function destroy(PurchaseOrder $po)
    {
        $po->delete();
        return redirect()->route('sales.purchase-orders')->with('success', 'Purchase Order deleted!');
    }
}
