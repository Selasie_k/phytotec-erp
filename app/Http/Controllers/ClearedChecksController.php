<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;

class ClearedChecksController extends Controller
{
    public function store(Payment $payment)
    {
        $payment->update(['check_cleared_on' => now()]);

        return redirect()->back()->with('success', 'Payment record updated! Check cleared.');
    }
}
