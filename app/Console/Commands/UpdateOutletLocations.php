<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\SalesOutlet;
use Illuminate\Console\Command;

class UpdateOutletLocations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'erp:update-outlet-locations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cut and append sales outlet suburb to the location column';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        SalesOutlet::all()->each(function($so){
            $this->newLine(2);
            $this->line("updating $so->name");
            $suburb = $so->suburb ? ", $so->suburb" : '';
            $so->update([
                'location' =>  $so->location . $suburb, 
                'suburb' => '',
                'products_stocking' => [],
            ]);
            $this->info('Update successful!');
        });
        return 0;
    }
}
