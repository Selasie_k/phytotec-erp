<?php

namespace App\Listeners;

use App\Events\PurchaseOrderCreated;
use App\Models\User;
use App\Notifications\PurchaseOrderNotificationToStaff;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyStaffOfNewPurchaseOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PurchaseOrderCreated  $event
     * @return void
     */
    public function handle(PurchaseOrderCreated $event)
    {
        User::shouldBeNotifiedOfEvent(get_class($event))
                ->each->notify(new PurchaseOrderNotificationToStaff($event->purchaseOrder));
    }
}
