<?php

namespace App\Listeners;

use App\Events\PurchaseRequestCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyStaffOfNewPurchaseRequest
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PurchaseRequestCreated  $event
     * @return void
     */
    public function handle(PurchaseRequestCreated $event)
    {
        //
    }
}
