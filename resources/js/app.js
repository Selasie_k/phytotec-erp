require('./bootstrap');

// require('alpinejs');
import Vue from 'vue'
import VueMeta from 'vue-meta'
import PortalVue from 'portal-vue'
import { App, plugin } from '@inertiajs/inertia-vue'
import { InertiaProgress } from '@inertiajs/progress/src'
import moment from 'moment'

require('./vue-tailwind');
import datePicker from 'vue-ctk-date-time-picker';
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';
Vue.component('date-time-picker', datePicker);

Vue.config.productionTip = false
Vue.mixin({ methods: { route: window.route } })
Vue.use(plugin)
Vue.use(PortalVue)
Vue.use(VueMeta)

Vue.prototype.$moment = moment
Vue.prototype.$window = window
Vue.prototype.$document = document

InertiaProgress.init()

const el = document.getElementById('app')

new Vue({
  metaInfo: {
    titleTemplate: (title) => title ? `${title} - PHYTOTEC ERP` : 'PHYTOTEC ERP'
  },
  render: h => h(App, {
    props: {
      initialPage: JSON.parse(el.dataset.page),
      resolveComponent: name => import(`@/Pages/${name}`).then(module => module.default),
    },
  }),
}).$mount(el)

Vue.prototype.$can = function(abilities){
  var permissions = JSON.parse(el.dataset.page).props.auth.user?.permissions
  // console.log(permissions)
  if(!permissions){
    return false
  }
  if(typeof abilities == 'string'){
    abilities = [abilities]
  }

  return !!(permissions.filter(i => abilities.includes(i)).length)
}

import Icon from './Shared/Icon'
Vue.component('icon', Icon)

import pagination from './Shared/Pagination'
Vue.component('pagination', pagination)

import FormSection from './Shared/FormSection'
Vue.component('form-section', FormSection)

import SectionLabel from './Shared/SectionLabel'
Vue.component('section-label', SectionLabel)

Vue.prototype.user = function() {
  return JSON.parse(el.dataset.page).props.auth.user || null
}

// console.log(JSON.parse(el.dataset.page).props.auth.user?.permissions)
// console.log(!!(['a', 'b', 'c'].filter(i => ['c', 'b'].includes(i)).length))
// var func = function(...params){
//   console.log(params)
// }
// console.log(func(['sss']))
