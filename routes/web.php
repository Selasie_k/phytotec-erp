<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\LineItemController;
use App\Http\Controllers\PlatformController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CallRecordController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\SalesOutletController;
use App\Http\Controllers\ClearedChecksController;
use App\Http\Controllers\PurchaseOrderController;
use App\Http\Controllers\HealthFacilityController;
use App\Http\Controllers\PurchaseRequestController;
use App\Http\Controllers\ClientEngagementController;
use App\Http\Controllers\ImpersonatedUserController;
use App\Http\Controllers\MedicalEngagementController;
use App\Http\Controllers\SampleDistributionController;
use App\Http\Controllers\MedicalProfessionalController;
use App\Http\Controllers\FacilityProfessionalController;
use App\Http\Controllers\ConfirmedPurchaseOrderController;
use App\Http\Controllers\ProductSurveyController;
use App\Http\Controllers\ReportsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'index'])
    ->name('dashboard')
    ->middleware(['auth']);

Route::get('/dashboard', [DashboardController::class, 'index'])
    ->name('dashboard')
    ->middleware(['auth']);

Route::prefix('/call-records')->middleware('auth')->group(function () {
    Route::get('/', [CallRecordController::class, 'index'])
        ->middleware('permission:calls:view')
        ->name('call-records');

    Route::get('/create', [CallRecordController::class, 'create'])
        ->middleware('permission:calls:create')
        ->name('call-records.create');

    Route::post('/', [CallRecordController::class, 'store'])
        ->name('call-records.store');

    Route::post('/{caller}/update', [CallRecordController::class, 'update'])
        ->middleware('permission:calls:update')
        ->name('call-records.update');

    Route::get('/{caller}', [CallRecordController::class, 'edit'])
        ->middleware('permission:calls:show')
        ->name('call-records.edit');

    Route::delete('/{caller}', [CallRecordController::class, 'destroy'])
        ->middleware('permission:calls:delete')
        ->name('call-records.destroy');
});

Route::prefix('/sales-outlets')->middleware('auth')->group(function () {
    Route::get('/', [SalesOutletController::class, 'index'])
        ->middleware('permission:outlets:view')
        ->name('outlets');

    Route::get('/create', [SalesOutletController::class, 'create'])
        ->middleware('permission:outlets:create')
        ->name('outlets.create');

    Route::post('/', [SalesOutletController::class, 'store'])
        ->name('outlets.store');

    Route::get('/{outlet}', [SalesOutletController::class, 'edit'])
        ->middleware('permission:outlets:show')
        ->name('outlets.edit');

    Route::post('/{outlet}/update', [SalesOutletController::class, 'update'])
        ->middleware('permission:outlets:update')
        ->name('outlets.update');

    Route::delete('/{outlet}', [SalesOutletController::class, 'destroy'])
        ->middleware('permission:outlets:delete')
        ->name('outlets.destroy');
});

Route::prefix('/marketing')->middleware('auth')->group(function () {
    Route::prefix('/client-engagements')->group(function () {
        Route::get('/', [ClientEngagementController::class, 'index'])
            ->middleware('permission:engagements:view')
            ->name('marketing.engagements');

        Route::get('/{outlet}/create', [ClientEngagementController::class, 'create'])
            ->name('marketing.engagements.create');

        Route::get('/{engagement}/edit', [ClientEngagementController::class, 'edit'])
            ->middleware('permission:engagements:show')
            ->name('marketing.engagements.edit');

        Route::post('/', [ClientEngagementController::class, 'store'])
            ->middleware('permission:engagements:create')
            ->name('marketing.engagements.store');

        Route::post('/{engagement}', [ClientEngagementController::class, 'update'])
            ->middleware('permission:engagements:update')
            ->name('marketing.engagements.update');

        Route::delete('/{engagement}', [ClientEngagementController::class, 'destroy'])
            ->middleware('permission:engagements:delete')
            ->name('marketing.engagements.destroy');
    });

    Route::prefix('/purchase-requests')->group(function () {
        Route::get('/', [PurchaseRequestController::class, 'index'])
            ->middleware('permission:purchase-requests:view')
            ->name('marketing.purchase-requests');

        Route::get('/create', [PurchaseRequestController::class, 'create'])
            ->name('marketing.purchase-requests.create');

        Route::post('/', [PurchaseRequestController::class, 'store'])
            ->middleware('permission:purchase-requests:create')
            ->name('marketing.purchase-requests.store');

        Route::get('/{purchaseRequest}/edit', [PurchaseRequestController::class, 'edit'])
            ->middleware('permission:purchase-requests:show')
            ->name('marketing.purchase-requests.edit');

        Route::post('/{purchaseRequest}', [PurchaseRequestController::class, 'update'])
            ->middleware('permission:purchase-requests:update')
            ->name('marketing.purchase-requests.update');

        Route::delete('/{purchaseRequest}', [PurchaseRequestController::class, 'destroy'])
            ->middleware('permission:purchase-requests:delete')
            ->name('marketing.purchase-requests.destroy');
    });

    Route::prefix('/survey')->middleware('auth')->group(function () {
        Route::get('/', [ProductSurveyController::class, 'index'])
            ->middleware('permission:calls:view')
            ->name('marketing.survey');
    
        Route::get('/create', [ProductSurveyController::class, 'create'])
            ->middleware('permission:calls:create')
            ->name('marketing.survey.create');
    
        Route::post('/', [ProductSurveyController::class, 'store'])
            ->name('marketing.survey.store');
    
        Route::post('/{survey}/update', [ProductSurveyController::class, 'update'])
            ->middleware('permission:calls:update')
            ->name('marketing.survey.update');
    
        Route::get('/{survey}', [ProductSurveyController::class, 'edit'])
            ->middleware('permission:calls:show')
            ->name('marketing.survey.edit');
    
        Route::delete('/{survey}', [ProductSurveyController::class, 'destroy'])
            ->middleware('permission:calls:delete')
            ->name('marketing.survey.destroy');
    });
});

Route::prefix('/sample-distribution')->group(function () {
    Route::get('/', [SampleDistributionController::class, 'index'])
        ->middleware('permission:samples:view')
        ->name('samples');

    Route::get('/create', [SampleDistributionController::class, 'create'])
        ->name('samples.create');

    Route::post('/', [SampleDistributionController::class, 'store'])
        ->middleware('permission:samples:create')
        ->name('samples.store');

    Route::get('/{sample}/edit', [SampleDistributionController::class, 'edit'])
        ->middleware('permission:samples:show')
        ->name('samples.edit');

    Route::post('/{sample}', [SampleDistributionController::class, 'update'])
        ->middleware('permission:samples:update')
        ->name('samples.update');

    Route::delete('/{sample}', [SampleDistributionController::class, 'destroy'])
        ->middleware('permission:samples:delete')
        ->name('samples.destroy');
});

Route::prefix('/medical')->middleware('auth')->group(function () {
    Route::prefix('/health-facilities')->group(function () {
        Route::get('/', [HealthFacilityController::class, 'index'])
            ->middleware('permission:health-facilities:view')
            ->name('medical.facilities');

        Route::get('/create', [HealthFacilityController::class, 'create'])
            // ->middleware('permission:health-facilities:create')
            ->name('medical.facilities.create');

        Route::post('/', [HealthFacilityController::class, 'store'])
            ->middleware('permission:health-facilities:create')
            ->name('medical.facilities.store');

        Route::get('/{facility}/edit', [HealthFacilityController::class, 'edit'])
            ->middleware('permission:health-facilities:show')
            ->name('medical.facilities.edit');

        Route::post('/{facility}', [HealthFacilityController::class, 'update'])
            ->middleware('permission:health-facilities:update')
            ->name('medical.facilities.update');

        Route::delete('/{facility}', [HealthFacilityController::class, 'destroy'])
            ->middleware('permission:health-facilities:delete')
            ->name('medical.facilities.destroy');
    });

    Route::prefix('/medical-professionals')->group(function () {
        Route::get('/', [MedicalProfessionalController::class, 'index'])
            ->middleware('permission:medical-professionals:view')
            ->name('medical.professionals');

        Route::get('/create', [MedicalProfessionalController::class, 'create'])
            // ->middleware('permission:medical-professionals:create')
            ->name('medical.professionals.create');

        Route::post('/', [MedicalProfessionalController::class, 'store'])
            ->middleware('permission:medical-professionals:create')
            ->name('medical.professionals.store');

        Route::get('/{professional}/edit', [MedicalProfessionalController::class, 'edit'])
            ->middleware('permission:medical-professionals:show')
            ->name('medical.professionals.edit');

        Route::post('/{professional}', [MedicalProfessionalController::class, 'update'])
            ->middleware('permission:medical-professionals:update')
            ->name('medical.professionals.update');

        Route::delete('/{professional}', [MedicalProfessionalController::class, 'destroy'])
            ->middleware('permission:medical-professionals:delete')
            ->name('medical.professionals.destroy');
    });

    Route::prefix('/medical-engagements')->group(function () {
        Route::get('/', [MedicalEngagementController::class, 'index'])
            ->middleware('permission:medical-engagements:view')
            ->name('medical.engagements');

        Route::get('/{professional}/create', [MedicalEngagementController::class, 'create'])
            // ->middleware('permission:medical-engagements:create')
            ->name('medical.engagements.create');

        Route::post('/', [MedicalEngagementController::class, 'store'])
            ->middleware('permission:medical-engagements:create')
            ->name('medical.engagements.store');

        Route::get('/{engagement}/edit', [MedicalEngagementController::class, 'edit'])
            ->middleware('permission:medical-engagements:show')
            ->name('medical.engagements.edit');

        Route::post('/{engagement}', [MedicalEngagementController::class, 'update'])
            ->middleware('permission:medical-engagements:update')
            ->name('medical.engagements.update');

        Route::delete('/{engagement}', [MedicalEngagementController::class, 'destroy'])
            ->middleware('permission:medical-engagements:delete')
            ->name('medical.engagements.destroy');
    });

    Route::prefix('/facility-professionals')->group(function () {
        Route::post('/{professional}', [FacilityProfessionalController::class, 'store'])
                ->name('facility-professionals.store');

        Route::delete('/{professional}', [FacilityProfessionalController::class, 'destroy'])
                ->name('facility-professionals.destroy');
    });
});

Route::prefix('/sales')->middleware('auth')->group(function () {
    Route::prefix('/purchase-orders')->group(function () {
        Route::get('/', [PurchaseOrderController::class, 'index'])
                ->middleware('permission:purchase-orders:view')
                ->name('sales.purchase-orders');

        Route::get('/{outlet}', [PurchaseOrderController::class, 'create'])
                // ->middleware('permission:purchase-orders:create')
                ->name('sales.purchase-orders.create');

        Route::post('/', [PurchaseOrderController::class, 'store'])
                ->middleware('permission:purchase-orders:create')
                ->name('sales.purchase-orders.store');

        Route::get('/{po}/{outlet}/edit', [PurchaseOrderController::class, 'edit'])
                ->middleware('permission:purchase-orders:show')
                ->name('sales.purchase-orders.edit');

        Route::post('/{po}/update', [PurchaseOrderController::class, 'update'])
                ->middleware('permission:purchase-orders:update')
                ->name('sales.purchase-orders.update');

        Route::delete('/{po}', [PurchaseOrderController::class, 'destroy'])
                ->middleware('permission:purchase-orders:delete')
                ->name('sales.purchase-orders.destroy');

        Route::get('/{po}/confirm', [ConfirmedPurchaseOrderController::class, 'store'])
                ->middleware('permission:purchase-orders:confirm')
                ->name('sales.purchase-orders.confirm');

        Route::prefix('/line-items')->group(function () {
            Route::post('/{po}', [LineItemController::class, 'store'])
                ->name('sales.purchase-orders.line-items.store');

            Route::delete('/{li}', [LineItemController::class, 'destroy'])
                ->name('sales.purchase-orders.line-items.destroy');
        });
    });
    
    Route::prefix('/payments')->group(function () {
        Route::get('/', [PaymentController::class, 'index'])
                ->middleware('permission:payments:view')
                ->name('sales.payments');

        Route::get('/{outlet}', [PaymentController::class, 'create'])
                ->name('sales.payments.create');

        Route::post('/', [PaymentController::class, 'store'])
                ->middleware('permission:payments:create')
                ->name('sales.payments.store');

        Route::get('/{payment}/edit/{outlet?}', [PaymentController::class, 'edit'])
                ->middleware('permission:payments:show')
                ->name('sales.payments.edit');

        Route::post('/{payment}/update', [PaymentController::class, 'update'])
                ->middleware('permission:payments:update')
                ->name('sales.payments.update');

        Route::delete('/{payment}', [PaymentController::class, 'destroy'])
                ->middleware('permission:payments:delete')
                ->name('sales.payments.destroy');

        Route::get('/{payment}/clear-check', [ClearedChecksController::class, 'store'])
                // ->middleware('permission:payments:clear-check')
                ->name('sales.payments.checks.clear');
    });
});

Route::prefix('/admin')->middleware(['auth', 'role:Sys-Admin'])->group(function () {
    Route::prefix('/users')->group(function () {
        Route::get('/', [UserController::class, 'index'])->name('admins.users');
        Route::get('/create', [UserController::class, 'create'])->name('admins.users.create');
        Route::post('/store', [UserController::class, 'store'])->name('admins.users.store');
        Route::get('/{user}/edit', [UserController::class, 'edit'])->name('admins.users.edit');
        Route::post('/{user}/update', [UserController::class, 'update'])->name('admins.users.update');
        Route::delete('/{user}/destroy', [UserController::class, 'destroy'])->name('admins.users.destroy');
        Route::get('/{user}/impersonate', ImpersonatedUserController::class)->name('admins.users.impersonate');
    });

    Route::prefix('/access-control')->group(function () {
        Route::get('/', [RoleController::class, 'index'])->name('admins.acl');

        Route::prefix('/roles')->group(function () {
            Route::post('/', [RoleController::class, 'store'])->name('admins.acl.roles.store');
            Route::get('/{role}', [RoleController::class, 'edit'])->name('admins.acl.roles.edit');
            // Route::patch('/{role}/update', [RoleController::class, 'update'])->name('admins.acl.roles.update');
            Route::delete('/{role}', [RoleController::class, 'destroy'])->name('admins.acl.roles.destroy');
        });

        Route::prefix('/permissions')->group(function () {
            Route::get('/', [PermissionController::class, 'create'])->name('admins.acl.permissions.create');
        });
    });

    Route::prefix('/platform')->group(function () {
        Route::get('/', [PlatformController::class, 'index'])->name('admins.platform');
    });
});

Route::prefix('/reports')->middleware('permission:reports:view-all')->group(function () {
    Route::get('/sales', [ReportsController::class, 'sales'])->name('reports.sales');

    Route::get('/samples', [ReportsController::class, 'samples'])->name('reports.samples');

    Route::get('/marketing', [ReportsController::class, 'marketing'])->name('reports.marketing');
});

require __DIR__.'/auth.php';
